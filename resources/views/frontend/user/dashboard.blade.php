@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.frontend.dashboard') )

@section('content')
    <div class="row no-gutters mb-4">
        <div class="col-lg-12 tabs-wrapper">
            <b-tabs content-class="m-3" nav-wrapper-class="tabs-orange" justified>
                <b-tab active>
                    <template slot="title">
                        <div class="navigation-title">
                            <i class=" icons icon-camrecorder"></i>
                            <div class="navigation-title__text">Стрим</div>
                        </div>
                    </template>
                    <b-card-text>
                        <div class="row">
                            <div class="col mb-4">
                                <video
                                    id="doterra-video"
                                    class="video-js vjs-default-skin"
                                    controls
                                    autoplay
                                    width="640" height="480"
                                    data-setup='{ "techOrder": ["youtube"], "sources": [{ "type": "video/youtube", "src": "https://youtu.be/BSxx2Yk-yOU"}] }'
                                >
                                </video>
                            </div>
                        </div><!--row-->
                    </b-card-text>
                </b-tab>
                <b-tab>
                    <template slot="title">
                        <div class="navigation-title">
                            <i class="icons icon-calendar"></i>
                            <div class="navigation-title__text">Расписание</div>
                        </div>
                    </template>
                    <b-card-text>
                        <div class="timetable">
                            <div class="timetable__date">17 декабря, 2018</div>
                            <div class="timeteable__section">
                                <div>09:30</div>
                                <div>Приветственный кофе</div>
                            </div>
                            <div class="timeteable__section">
                                <div class="timetable__time">10:00</div>
                                <div>Введение в тему: план работы, основные тренды в обучении</div>
                                <div>Спикер: Володин Александр Владимирович</div>
                                <div>Директор по развитию</div>
                            </div>
                            <div class="timeteable__section">
                                <div>10:30</div>
                                <div>Практическая работа: обсуждение портрета современного слушателя</div>
                                <div>Спикер: Володин Александр Владимирович</div>
                                <div>Директор по развитию</div>
                            </div>
                        </div>
                    </b-card-text>
                </b-tab>
                <b-tab>
                    <template slot="title">
                        <div class="navigation-title">
                            <i class="icons icon-bubbles"></i>
                            <div class="navigation-title__text">Чат</div>
                        </div>
                    </template>
                    <b-card-text>
                        <chat-messages :messages="messages"></chat-messages>
                        <chat-form
                            v-on:messagesent="addMessage"
                            :user="{{ Auth::user() }}"
                        ></chat-form>
                    </b-card-text>
                </b-tab>
            </b-tabs>
        </div>
    </div><!-- row -->
@endsection
